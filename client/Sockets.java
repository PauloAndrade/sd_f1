//Paulo Andrade 81625; Daniel Cruz 80812; Hélder Barbosa 82861;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sd_f1.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author woah
 */
public class Sockets {

    Socket ligacaoSocket = null;
    InetAddress endereco;
    int porto;

    public Sockets(String servidor, int porto) {
        endereco = null;
        try {
            endereco = InetAddress.getByName(servidor);
            this.porto = porto;
        } catch (UnknownHostException e) {
            System.out.println("Endereco desconhecido: " + e);
            System.exit(-1);
        }
    }

    public byte[] trocarMensagem(byte dataOut[]) {
        try {
            ligacaoSocket = new Socket(endereco, porto);
        } catch (Exception e) {
            System.err.println("erro ao criar socket...");
            e.printStackTrace();
            System.exit(-1);
        }
        byte[] dataIn = new byte[50];
        try {
            InputStream inputStream = ligacaoSocket.getInputStream();
            OutputStream outputStream = ligacaoSocket.getOutputStream();
            outputStream.write(dataOut);
            outputStream.flush();
            inputStream.read(dataIn);
            inputStream.close();
            outputStream.close();
            ligacaoSocket.close();
        } catch (IOException e) {
            System.out.println("Erro ao comunicar com o servidor: " + e);
            System.exit(1);
        }
        return dataIn;
    }
}
