//Paulo Andrade 81625; Daniel Cruz 80812; Hélder Barbosa 82861;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sd_f1.client;

import java.io.UnsupportedEncodingException;
import static java.lang.Thread.sleep;
import java.rmi.RemoteException;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author Paulo Andrade 81625; Daniel Cruz 80812; Hélder Barbosa 82861;
 */
public class UserInterface {

    public static void main(String[] args) throws UnsupportedEncodingException, InterruptedException, RemoteException {
        clienteInterface cliente;
        cliente = new Cliente();
        System.out.println("\nEste é o prompt do Cliente F1 \n"
                + " Para definir o nome do Cliente use: \"name [nome]\""
                + "\n Para fazer uma ultrapassagem use:"
                + "\"overtake [ultrapassante] [ultrapassado] [volta]\""
                + "\n Para atualizar use: \"updateClient\""
                + "\n Para usar RMI use: \"useRMI\""
                + "\n Para sair use: \"quit\"");
        String input, command;
        StringTokenizer st;
        int ultrapassado, ultrapassante, volta;
        try (Scanner reader = new Scanner(System.in)) {
            while (true) {
                System.out.print("➜ ");
                while (!reader.hasNextLine()) {
                    sleep(100);
                }
                input = reader.nextLine();
                st = new StringTokenizer(input);
                if (st.hasMoreTokens()) {
                    command = st.nextToken().toLowerCase();
                    switch (command) {
                        case "name":
                            if (st.hasMoreTokens()) {
                                String nome = st.nextToken();
                                cliente.setNome(nome);
                                System.out.println("Nome assimilado: " + nome);
                            } else {
                                System.out.println("Nome não assimilado");
                            }
                            break;
                        case "overtake":
                            if (st.hasMoreTokens()) {
                                ultrapassante = Integer.parseInt(st.nextToken());
                                if (st.hasMoreTokens()) {
                                    ultrapassado = Integer.parseInt(st.nextToken());
                                    if (st.hasMoreTokens()) {
                                        volta = Integer.parseInt(st.nextToken());
                                        cliente.ultrapassagem(ultrapassante, ultrapassado, volta);
                                        System.out.println(
                                                "Ultrapassagem por " + ultrapassante
                                                + " ao " + ultrapassado
                                                + " na volta " + volta);
                                    } else {
                                        System.out.println("Argumento 3 Inválido");
                                    }
                                } else {
                                    System.out.println("Argumento 2 Inválido");
                                }
                            } else {
                                System.out.println("Argumento 1 Inválido");
                            }
                            break;
                        case "updateclient":
                            cliente.atualizacao();
                            System.out.println("Updated");
                            break;
                        case "usermi":
                            cliente = new ClienteRMI();
                            System.out.println("A utilizar RMI");
                            break;
                        case "quit":
                            System.out.println("GoodBye!!!");
                            System.exit(0);
                            break;
                        default:
                            System.out.println("Comando não Reconhecido: " + input);
                            break;
                    }
                }
            }
        }
    }
}
