/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sd_f1.client;

import java.io.UnsupportedEncodingException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import sd_f1.utils.*;
import sd_f1.utils.BinaryArrayList;
import sd_f1.utils.Corrida;

/**
 *
 * @author woah
 */
public class ClienteRMI extends UnicastRemoteObject implements clienteInterface{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String servidor, nome;
    int porto, updates, caracteres;

    public ClienteRMI() throws UnsupportedEncodingException, RemoteException {
        servidor = "127.0.0.1";
        updates = 0;
        nome = "Cliente Sem Nome";
    }

    public void ultrapassagem(int ultrapassante, int ultrapassado, int volta) throws UnsupportedEncodingException {
        trocarMensagem(ultrapassante, ultrapassado, volta, (this.updates));
    }

    public void atualizacao() throws UnsupportedEncodingException {
        trocarMensagem(0, 0, 0, 0);
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    private void trocarMensagem(int ultrapassante, int ultrapassado, int volta, int update) throws UnsupportedEncodingException {
        try {
            String endereco= "rmi://" + servidor + "/corrida";
            corridaRemotoInterface stub = (corridaRemotoInterface) Naming.lookup(endereco);                                 
            Classificacao retorno = stub.sincronizarClassificacao(ultrapassante, ultrapassado, volta, nome, update);
            System.out.println("response: ");
            BinaryArrayList binaryArrayListIn = new BinaryArrayList();
            binaryArrayListIn.importArray(retorno.getData());
            System.out.println("Retorno: "
                    + "\nNúmero 1 " + binaryArrayListIn.get(0)
                    + "\nNúmero 2 " + binaryArrayListIn.get(1)
                    + "\nNúmero 3 " + binaryArrayListIn.get(2)
                    + "\nNúmero 4 " + binaryArrayListIn.get(3)
                    + "\nNúmero 5 " + binaryArrayListIn.get(4)
                    + "\nNúmero 6 " + binaryArrayListIn.get(5)
                    + "\nvolta " + binaryArrayListIn.get(6)
                    + "\nNúmero de Atualizações " + binaryArrayListIn.get(8)
            );
            updates = (int) binaryArrayListIn.get(8);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
