package sd_f1.client;

import java.io.UnsupportedEncodingException;

public interface clienteInterface {

    public void ultrapassagem(int ultrapassante, int ultrapassado, int volta)throws UnsupportedEncodingException;

    public void atualizacao()throws UnsupportedEncodingException;

    public void setNome(String nome)throws UnsupportedEncodingException;
}
