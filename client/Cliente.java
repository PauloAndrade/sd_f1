//Paulo Andrade 81625; Daniel Cruz 80812; Hélder Barbosa 82861;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sd_f1.client;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import sd_f1.utils.BinaryArrayList;

/**
 *
 * @author woah
 */
public class Cliente implements clienteInterface{

    String servidor, nome;
    int porto, updates, caracteres;
    InetAddress endereco;
    Sockets sockets;

    public Cliente() throws UnsupportedEncodingException {
        servidor = "127.0.0.1";
        porto = 8082;
        sockets = new Sockets(servidor, porto);
        updates = 0;
        nome = "Cliente Sem Nome";
    }

    public void ultrapassagem(int ultrapassante, int ultrapassado, int volta) throws UnsupportedEncodingException {
        trocarMensagem(ultrapassante, ultrapassado, volta, (this.updates + 1));
    }

    public void atualizacao() throws UnsupportedEncodingException {
        trocarMensagem(0, 0, 0, -1);
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    private void trocarMensagem(int ultrapassante, int ultrapassado, int volta, int update) throws UnsupportedEncodingException {
        BinaryArrayList binaryArrayListOut = new BinaryArrayList();
        binaryArrayListOut.add(ultrapassante);
        binaryArrayListOut.add(ultrapassado);
        binaryArrayListOut.add(volta);
        binaryArrayListOut.add(this.nome.length());
        binaryArrayListOut.add(this.nome);
        binaryArrayListOut.add(0);
        binaryArrayListOut.add(update + 1);
        binaryArrayListOut.print();
        byte[] retorno = sockets.trocarMensagem(binaryArrayListOut.toByteArray());
        BinaryArrayList binaryArrayListIn = new BinaryArrayList();
        binaryArrayListIn.importBytes(retorno, 0, retorno.length-1);
        System.out.println("Retorno: "
                + "\nNúmero 1 " + binaryArrayListIn.get(0)
                + "\nNúmero 2 " + binaryArrayListIn.get(1)
                + "\nNúmero 3 " + binaryArrayListIn.get(2)
                + "\nNúmero 4 " + binaryArrayListIn.get(3)
                + "\nNúmero 5 " + binaryArrayListIn.get(4)
                + "\nNúmero 6 " + binaryArrayListIn.get(5)
                + "\nvolta " + binaryArrayListIn.get(6)
                + "\nNúmero de Atualizações " + binaryArrayListIn.get(8)
        );
        updates =(int) binaryArrayListIn.get(8);

    }
}
