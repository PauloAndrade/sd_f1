//Paulo Andrade 81625; Daniel Cruz 80812; Hélder Barbosa 82861;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sd_f1.utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 *
 * @author woah
 */
public class BinaryArrayList {

    ArrayList<Object> binaryArrayList;

    public BinaryArrayList() {
        binaryArrayList = new ArrayList<>();
    }

    public void add(Object obj) {
        this.binaryArrayList.add(obj);
    }

    public Object get(int i) {
        return this.binaryArrayList.get(i);
    }

    public int size() {
        return this.binaryArrayList.size();
    }

    public byte[] getBytes(int index) throws UnsupportedEncodingException {
        byte[] byteArray;
        if (this.binaryArrayList.get(index) instanceof String) {
            String value = this.binaryArrayList.get(index).toString();
            byteArray = value.getBytes("US-ASCII");
        } else if (this.binaryArrayList.get(index) instanceof Integer) {
            int value = (int) this.binaryArrayList.get(index);
            byteArray = new byte[1];
            byteArray[0] = (byte) value;
        } else {
            System.out.println("non valid BinaryArrayList element");
            byteArray = new byte[0];

        }
        return byteArray;
    }

    public byte[] toByteArray() throws UnsupportedEncodingException {
        byte[] byteArrayData = new byte[0];
        for (int i = 0; i < this.binaryArrayList.size(); i++) {
            byteArrayData = combineByteArray(byteArrayData, getBytes(i));
        }
        return byteArrayData;
    }

    public byte[] combineByteArray(byte[] a, byte[] b) {
        byte[] result = new byte[a.length + b.length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

    public void importBytes(byte[] byteArrayData, int indexStart, int indexEnd) {
        for (int i = indexStart; i <= indexEnd; i++) {
            this.add(new Byte(byteArrayData[i]).intValue());
        }
    }

    public void importArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
    }

    public void importAsciiBytes(byte[] byteArrayData, int indexStart, int indexEnd) {
        char[] charArray = new char[indexEnd - indexStart + 1];
        for (int i = indexStart; i <= indexEnd; i++) {
            charArray[i - indexStart] = (char) byteArrayData[i];
        }
        this.add(new String(charArray));
    }

    public void print() {
        System.out.println(this.binaryArrayList.toString());
    }
}
