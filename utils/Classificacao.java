//Paulo Andrade 81625; Daniel Cruz 80812; Hélder Barbosa 82861;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sd_f1.utils;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.Serializable;

/**
 *
 * @author woah
 */
public class Classificacao  implements Serializable {
	private static final long serialVersionUID = 1L;
	public int[] data;

	public Classificacao() throws RemoteException {
	}

	public int[] getData() {
		return data;
	}

	public void setData(int[] data) {
		this.data = data;
	}
}