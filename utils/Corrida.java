//Paulo Andrade 81625; Daniel Cruz 80812; Hélder Barbosa 82861;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sd_f1.utils;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.Serializable;

/**
 *
 * @author woah
 */
public class Corrida extends UnicastRemoteObject implements Serializable{
        /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		int volta, atualizacoes;
		
    ArrayList<Integer> list = new ArrayList<>();
    ArrayList<String> Nome = new ArrayList<>();
    ArrayList<String> Data = new ArrayList<>();

    public Corrida() throws RemoteException{
        this.volta = 0;
        this.atualizacoes = 0;
        list.addAll(Arrays.asList(21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40));
    }

    public int getVolta() {
		return volta;
	}

	public int getAtualizacoes() {
		return atualizacoes;
	}

	public ArrayList<Integer> getList() {
		return list;
	}

	public void ultrapassagem(int ultrapassante, int ultrapassado, int volta, int updates,String nome, String data) {
        if (volta >= this.volta && updates > this.atualizacoes && list.contains(ultrapassante) && list.contains(ultrapassado) && updates > this.atualizacoes) {
            int indiceUltrapassante = list.indexOf(ultrapassante);//&& updates > this.atualizacoes
            int indiceUltrapassado = list.indexOf(ultrapassado);
            list.remove(indiceUltrapassante);
            list.add(indiceUltrapassado, ultrapassante);
            this.volta=volta;
            this.atualizacoes++;
            Nome.add(nome);
            Data.add(data);
        }

    }

    public ArrayList<String> getNome() {
		return Nome;
	}

	public ArrayList<String> getData() {
		return Data;
	}

	public int[] atualizacao() {
        int[] data = {list.get(0),list.get(1),list.get(2),list.get(3),list.get(4),list.get(5),this.volta,0,this.atualizacoes};
           return data;
       
    }
}
