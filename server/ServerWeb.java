package sd_f1.server;

import java.net.*;
import java.io.*;
import java.util.*;

import sd_f1.utils.Corrida;

public class ServerWeb extends Thread {
	Socket ligacao;
	BufferedReader in;
	PrintWriter out;
	String array;
	int n_volta;
	int n_atualizacoes;
	ArrayList<String> arrayNick;
	ArrayList<String> arrayData;

	public ServerWeb(Socket ligacao, Corrida corrida) {
		this.ligacao = ligacao;
		try {
			this.in = new BufferedReader(new InputStreamReader(ligacao.getInputStream()));
			this.out = new PrintWriter(ligacao.getOutputStream());
		} catch (IOException e) {
			System.out.println("Erro na execucao do servidor: " + e);
			System.exit(1);
		}
		array = corrida.getList().subList(0, 6).toString();
		n_volta = corrida.getVolta();
		n_atualizacoes = corrida.getAtualizacoes();
		arrayNick = corrida.getNome();
		arrayData = corrida.getData();
	}

	public String handleRequest(String address) {
		String res = "";
		if (!address.isEmpty()) {
			StringTokenizer st = new StringTokenizer(address, "/");
			System.out.println("line 28");
			if (st.hasMoreTokens()) {
				switch (st.nextToken()) {
				case "CorridaFormula1":
					System.out.println("line 32");
					if (address.contains("resultado")) {
						res = "{ \"Resultado\": {\n" + "\"Top6\": " + array + ",\n" + "\"n_volta\": " + n_volta + ",\n"
								+ "\"n_atualizacoes\": " + n_atualizacoes + "\n" + "}\n" + "}";
						break;
					}
					if (address.contains("atualizacoes")) {
						int number = 0;
						while (st.hasMoreTokens()) {
							try {
								number = Integer.parseInt(st.nextToken());
							} catch (Exception e) {
								number = 10;
							}
						}
						String middle = " ";
						if (arrayNick.size() < number) {
							number = arrayNick.size();
						}
						for (int i = arrayNick.size() - number; i < arrayNick.size(); i++) {
							middle = middle + "{\n" + "\n\"nickname\":\"" + arrayNick.get(i) + "\",\n" + "\"data\":\""
									+ arrayData.get(i) + "\"\n},";
						}
						res = "{ \"Atualizacoes\" : [\n" + middle.substring(0, (middle.length() - 1)) + "]\n" + "}";
					}
					break;

				default:
					res = "";
					break;
				}
			}
			return res;
		} else {
			return "opsie";
		}
	}

	public void run() {
		try {
			String msg;
			String res;
			System.out.println("line 61");
			int len, length;
			String metodo = null;
			String address = "";
			msg = in.readLine();
			len = msg == null ? 0 : msg.trim().length();
			while (len != 0) {
				StringTokenizer tokens = new StringTokenizer(msg);
				String token = tokens.nextToken();
				if (token.equals("GET")) {
					metodo = "GET";
					address = tokens.nextToken();
				}
				msg = in.readLine();
				len = msg == null ? 0 : msg.trim().length();
			}
			System.out.println("line 76");
			if (metodo == "GET") {
				System.out.println("A ligacao que atendeu o GET foi: " + ligacao.getInetAddress() + " do porto "
						+ ligacao.getPort());
				System.out.println("GET");
				System.out.println("HTTP/1.1 200 OK");
				out.println("HTTP/1.1 200 OK");
				res = handleRequest(address);
				length = res.length();
				out.println("Content-type: application/json");
				out.println("Content-Length: " + length);
				out.write("\r\n");
				out.println(res);
				out.flush();
			} else {
				out.println("HTTP/1.1 404 Not Found");
			}
			in.close();
			out.close();
			ligacao.close();
		} catch (IOException e) {
			System.out.println("Erro na execucao do servidor: " + e);
			System.exit(1);
		}
	}
}