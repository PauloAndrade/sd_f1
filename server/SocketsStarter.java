package sd_f1.server;

import java.net.*;
import java.io.*;
import java.util.*;

import sd_f1.utils.Corrida;

public class SocketsStarter extends Thread {
	Socket ligacao;
	BufferedReader in;
	PrintWriter out;
	Corrida corrida;
	public SocketsStarter(Corrida corrida) {
		this.corrida= corrida;
	}
	@Override
	public void run() {
		int porto = 8082;
		Socket ligacao;
		ServerSocket servidor = null;
		try {
			servidor = new ServerSocket(porto);
		} catch (Exception e) {
			System.err.println("erro ao criar socket servidor...");
			e.printStackTrace();
			System.exit(-1);
		}
		while (true) {
			try {
				// Aguarda que seja estabelecida alguma ligacao e quando isso acontecer cria um
				// socket
				// chamado ligacao para atender essa liga��o
				ligacao = servidor.accept();
				
				ServerSockets serverSockets = new ServerSockets(ligacao, corrida);
				serverSockets.start();
				System.out.println("Liga��o iniciada");
			} catch (IOException e) {
				System.out.println("Erro na execucao do servidor: " + e);
				System.exit(1);
			}
		}
	}
}