//Paulo Andrade 81625; Daniel Cruz 80812; H�lder Barbosa 82861;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sd_f1.server;

import sd_f1.utils.Corrida;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

/**
 *
 * @author woah
 */
public class Server {

    public Server() {

    }

    public static void main(String[] args) throws RemoteException {
        Corrida corrida = new Corrida();
          //RMI
          System.getProperties().put("java.security.policy", "./server.policy");
        try {
            LocateRegistry.createRegistry(1099);
        } catch (RemoteException e) {
            System.out.println("RemoteException: "+e);
        }

        try {
            ServerRMI rmi = new ServerRMI(corrida);

            Naming.rebind("corrida", rmi);
        } catch (Exception e) {
            System.out.println("Exceptiion: " + e);
        }
          System.out.println("Servidor iniciado");
          //web
          WebStarter webStarter = new WebStarter(corrida);
          webStarter.start();
          SocketsStarter socketsStarter  = new SocketsStarter (corrida);
          socketsStarter .start();
    }
}

