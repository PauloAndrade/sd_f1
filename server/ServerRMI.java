/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sd_f1.server;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import sd_f1.utils.Classificacao;
import sd_f1.utils.Corrida;
import sd_f1.utils.corridaRemotoInterface;

/**
 *
 * @author Woah
 */
public class ServerRMI extends UnicastRemoteObject implements corridaRemotoInterface {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Corrida corrida;

    public ServerRMI(Corrida corrida)throws RemoteException {
        this.corrida = corrida;
    }

    @Override
    public Classificacao sincronizarClassificacao(int n_monolugar_que_ultrapassou, int n_monolugar_que_foi_ultrapassado, int n_volta, String nomeParticipante, int n_atualizacoes) throws RemoteException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
    	corrida.ultrapassagem(n_monolugar_que_ultrapassou, n_monolugar_que_foi_ultrapassado, n_volta, n_atualizacoes, nomeParticipante,dateFormat.format(date));
        System.out.println("Cliente Rmi: " + nomeParticipante);
        Classificacao retorno = new Classificacao();
        	retorno.setData(corrida.atualizacao());	;                                        
        return retorno;
    }

}
