//Paulo Andrade 81625; Daniel Cruz 80812; Hélder Barbosa 82861;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sd_f1.server;

import sd_f1.utils.Corrida;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import sd_f1.utils.BinaryArrayList;

/**
 *
 * @author woah
 */
public class ServerSockets extends Thread {

	Socket ligacaoSocket;
	InputStream inputStream;
	OutputStream outputStream;
	Corrida corrida;

	public ServerSockets(Socket ligacao, Corrida corrida) {
		this.ligacaoSocket = ligacao;
		this.corrida = corrida;
		try { // cria uma InputStream para ler os dados que chegam do socket
			this.inputStream = ligacao.getInputStream();
			this.outputStream = ligacao.getOutputStream();
		} catch (IOException e) {
			System.out.println("Erro na execucao do servidor: " + e);
			System.exit(1);
		}
	}

	@Override
	public void run() {
		try {
			while (!ligacaoSocket.isClosed()) {
				byte dataIn[] = new byte[200];
				inputStream.read(dataIn);
				ultrapassar(dataIn);
				outputStream.write(atualizar());
				outputStream.flush();
				outputStream.close();
				inputStream.close();
			}
			ligacaoSocket.close();
		} catch (IOException e) {
			System.out.println("Erro na execucao do servidor: " + e);
			System.exit(1);
		}
	}

	private byte[] atualizar() throws UnsupportedEncodingException {
		BinaryArrayList binaryArrayList = new BinaryArrayList();
		binaryArrayList.importArray(corrida.atualizacao());
		System.out.println("l60 atualizar");
		binaryArrayList.print();
		return binaryArrayList.toByteArray();
	}

	private void ultrapassar(byte[] dataIn) {
		BinaryArrayList binaryArrayList = new BinaryArrayList();
		binaryArrayList.importBytes(dataIn, 0, 3);
		int nameSize = (int) binaryArrayList.get(3);
		binaryArrayList.importAsciiBytes(dataIn, 4, nameSize + 3);
		binaryArrayList.importBytes(dataIn, nameSize + 3, nameSize + 5);
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		corrida.ultrapassagem((int) binaryArrayList.get(0), (int) binaryArrayList.get(1), (int) binaryArrayList.get(2),
				(int) binaryArrayList.get((int) binaryArrayList.size() - 1), binaryArrayList.get(4).toString(),
				dateFormat.format(date));
		System.out.println("l73 ultrapassar");
		binaryArrayList.print();
		System.out.println("Cliente:" + binaryArrayList.get(4) + " Atualização:"
				+ (int) binaryArrayList.get((int) binaryArrayList.size() - 1));
	}
}
